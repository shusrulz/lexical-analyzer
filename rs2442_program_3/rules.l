/*******************************************************************************
  Name: Rishav Sharma         NetID: rs2442
  Course: CSE 4713              Assignment: Program 3
  Programming Environment: Visual Studio C++ 
  Purpose of File: Contains the ....
*******************************************************************************/
%option noyywrap
%{
#include "lexer.h"

// global variable to hold current line number being read
int line_number = 1;

%}

%%

 /* Keywords */
if               { return TOK_IF;}
for              { return TOK_FOR;}
while            { return TOK_WHILE;}
return           { return TOK_RETURN;}
break            { return TOK_BREAK;}
read             { return TOK_READ;}
let              { return TOK_LET;}
else             { return TOK_ELSE;}
print            { return TOK_PRINT;}
continue         {return TOK_CONTINUE;}




 /* Datatype Specifiers */
 int   { return TOK_INT; }
 string { return TOK_STRING; }
 float { return TOK_FLOAT; }





 /* Punctuation */
\(             { return TOK_OPENPAREN; }
\)             { return TOK_CLOSEPAREN; }
\;             { return TOK_SEMICOLON; }
\{             { return TOK_OPENBRACE;}
\}             { return TOK_CLOSEBRACE;}



 /* Operators */

\+        { return TOK_PLUS; }
\-        { return TOK_MINUS; }
\*        { return TOK_MULTIPLY; }
\/        { return TOK_DIVIDE; }
\:=       { return TOK_ASSIGN; }
\==       { return TOK_EQUALTO; }
\<        { return TOK_LESSTHAN; }
\>        { return TOK_GREATERTHAN; }
\<>       { return TOK_NOTEQUALTO; }
and      { return TOK_AND; }
or       { return TOK_OR; }
not      { return TOK_NOT; }
length   { return TOK_LENGTH; }



 /* Abstractions */
[0-9]+                 { return TOK_INTLIT; }
[A-Za-z][0-9a-zA-Z_]*   { return TOK_IDENTIFIER; }
[0-9]+"."[0-9]*       { return TOK_FLOATLIT;}
\"[^\"]*\"             { return TOK_STRINGLIT;}
\"[^\"]+               { return TOK_EOF_SL;}




 /* Eat any whitespace */
[ \t\r]+             /* nop */

 /* Line incrementer */
[\n]              { line_number++; }

 /* Found an unknown character */

.         { return TOK_UNKNOWN; }

 /* Recognize end of file */

<<EOF>>   { return TOK_EOF; }


