//*****************************************************************************
// purpose: header file containing recursive descent parser productions
//  author: Joe Crumpton
//*****************************************************************************

#ifndef PRODUCTIONS_H
#define PRODUCTIONS_H


extern int nextToken;
extern int level;

extern "C"
{
	// Instantiate global variables used by flex
	extern char* yytext;       // text of current lexeme
	extern int   yylex();      // the generated lexical analyzer
}

// forward declaration of production functions
float expr();
float term();
float factor();
void program();
void statement();
void output();
void get();
void assignment();
void whilee();
void condition();
float boolean();
float relation();
float unary();



// forward declarations of first set checking functions
bool isFirstOfExpr();
bool isFirstOfTerm();
bool isFirstOfFactor();
bool isFirstOfProgram();
bool isFirstOfStatement();
bool isFirstOfOutput();
bool isFirstOfGet();
bool isFirstOfWhilee();
bool isFirstOfCondition();
bool isFirstOfAssignment();
bool isFirstOfBoolean();
bool isFirstOfRelation();
bool isFirstOfUnary();
// forward declaration of function used to help with tree nesting
string spaces();
typedef map<string, float> SymbolTableT;

//*****************************************************************************

SymbolTableT SymbolTable;

void program()
{
	static int programCount = 0;
	int currentprogramCount = ++programCount;

	char const* Terr =
		"<program> does not start with open curly brase '{'";

	level = level + 1;
	cout << spaces() << "enter <program> " << currentprogramCount << endl;
	cout << spaces() << "-->found " << yytext << endl;
	nextToken = yylex();

	while (isFirstOfStatement())
		statement();


	if (nextToken != TOK_CLOSEBRACE)
		throw "missing '}' at end of program";
	cout << "-->found " << yytext << endl;

	nextToken = yylex();
	cout << spaces() << "exit <program>" << currentprogramCount << endl;
	level = level - 1;


}
void statement() {
	static int statementCount = 0;
	int currentstatementCount = ++statementCount;

	char const* Terr =
		"statement does not start with 'let'| 'read' | 'print'|'if'|'while' ";
	level = level + 1;
	cout << spaces() << "enter <statement> " << currentstatementCount << endl;

	switch (nextToken) {
	case TOK_READ:
		get();
		break;
	case TOK_IF:
		condition();
		break;
	case TOK_LET:
		assignment();
		break;
	case TOK_WHILE:
		whilee();
		break;
	case TOK_PRINT:
		output();
		break;
	default:
		throw Terr;

	}

	

	cout << spaces() << "exit <statement> " << currentstatementCount << endl;
	level = level - 1;
}

void assignment() {
	static int assignmentCount = 0;
	int currentAssignmentCount = ++assignmentCount;
	float rtrnvalue;
	level = level + 1;

	cout << spaces() << "enter <assignment> " << currentAssignmentCount << endl;
	cout << spaces() << "-->found " << yytext << endl;

	nextToken = yylex();

	if (nextToken != TOK_IDENTIFIER){
		throw "missing identifier in assignment statement";
}
	cout <<spaces()<< "-->found ID: " << yytext << endl;
	string IDname = yytext;

	//checking for assignment ':='
	nextToken = yylex();
	if (nextToken != TOK_ASSIGN) {
		throw "missing ':=' in assignment statement";
	}
	cout <<spaces()<< "-->found " << yytext << endl;

	nextToken = yylex();
	if (isFirstOfExpr()) {
		rtrnvalue = expr();
	}
	else {
		throw "missing expression in assignment statement ";
	}

	SymbolTableT::iterator it = SymbolTable.find(IDname);
	if (it == SymbolTable.end()) {
		SymbolTable.insert(pair<string, float>(IDname, 1.0));
	}

	it = SymbolTable.find(IDname);
	it->second = rtrnvalue;

	//checking for ';'
	if (nextToken == TOK_SEMICOLON) {
		cout <<spaces()<< "-->found " << yytext << endl;

		nextToken = yylex();
	}
	else {
		throw "missing ';' at the end of the assignment statement";
	}
	cout << spaces() << "exit <assignment> " << currentAssignmentCount << endl;
	level = level - 1;

}



// <expr> --> <term> { ( + | - ) <term> }
float expr()
{
	float rtrnvalue1 = 0;
	float rtrnvalue2;

	static int exprCount = 0; // Count the number of <expr>'s
	int currentExprCount = ++exprCount;

	char const* Terr =
		"<term> does not start with 'IDENT' | 'INT_LIT' | '('";
	level = level + 1;
	cout << spaces() << "enter <expr> " << currentExprCount << endl;

	// checking of the first of boolean

	if (isFirstOfBoolean())
		rtrnvalue1 = boolean();
	else
		throw Terr;
	while (nextToken == TOK_AND || nextToken == TOK_OR)
	{
		cout << spaces() << "-->found " << yytext << endl;
		int nextTokenLast = nextToken;
		nextToken = yylex();
		if (isFirstOfBoolean())
			rtrnvalue2 = boolean();
		else
			throw Terr;
		switch (nextTokenLast)
		{

		case TOK_AND:
			rtrnvalue1 = rtrnvalue1 && rtrnvalue2;
			break;

		case TOK_OR:
			rtrnvalue1 = rtrnvalue1 || rtrnvalue2;
		}

	}

	cout << spaces() << "exit <expr> " << currentExprCount << endl;
	level = level - 1;

	return rtrnvalue1;
}
float boolean()
{
	float rtrnvalue1 = 0;
	float rtrnvalue2;
	static int booleanCount = 0;
	int currentBooleanCount = ++booleanCount;
	char const *Terr =
		"statement does not start with 'not' | '-' | '(' | 'ID' | 'INTLIT'";
	level = level + 1;
	cout << spaces() << "enter <boolean> " << currentBooleanCount << endl;

	//Checking for R
	if (isFirstOfRelation()) {
		rtrnvalue1 = relation();
	}
	else
		throw Terr;

	//Checking if there is a '<' or '>' or '==' followed by an R:
	if (nextToken == TOK_LESSTHAN || nextToken == TOK_GREATERTHAN || nextToken == TOK_EQUALTO)
	{
		cout << spaces() << "-->found " << yytext << endl;
		int nextTokenLast = nextToken;
		nextToken = yylex();
		if (isFirstOfRelation()) {
			rtrnvalue2 = relation();
		}
		else
			throw Terr;

		switch (nextTokenLast)
		{

		case TOK_LESSTHAN:
			rtrnvalue1 = rtrnvalue1 < rtrnvalue2;
			break;

		case TOK_GREATERTHAN:
			rtrnvalue1 = (rtrnvalue1 > rtrnvalue2);
			break;

		default:
			rtrnvalue1 = (rtrnvalue1 == rtrnvalue2);
		}

	}
	cout << spaces() << "exit <boolean> " << currentBooleanCount << endl;
	level = level - 1;
	return rtrnvalue1;

}
float relation()
{
	float rtrnvalue1 = 0;
	float rtrnvalue2;
	static int relationCount = 0;
	int currentRelationCount = ++relationCount;
	char const *Terr =
		"statement does not start with 'not' | '-' | '(' | 'ID' | 'INTLIT'";
	level = level + 1;
	cout << spaces() << "enter <relation> " << currentRelationCount << endl;


	if (isFirstOfTerm())
		rtrnvalue1 = term();
	else
		throw Terr;

	while (nextToken == TOK_PLUS || nextToken == TOK_MINUS)
	{
		cout << spaces() << "-->found " << yytext << endl;
		int nextTokenLast = nextToken;
		nextToken = yylex();
		if (isFirstOfTerm())
			rtrnvalue2 = term();
		else
			throw Terr;

		switch (nextTokenLast)
		{
		case TOK_PLUS:
			rtrnvalue1 = rtrnvalue1 + rtrnvalue2;
			break;

		case TOK_MINUS:
			rtrnvalue1 = rtrnvalue1 - rtrnvalue2;
		}

	}

	cout << spaces() << "exit <relation> " << currentRelationCount << endl;
	level = level - 1;
	return rtrnvalue1;
}






//*****************************************************************************
// <term> --> <factor> { ( * | / ) <factor> }
float term() {

	float rtrnvalue1 = 0;
	float rtrnvalue2;

	static int termCount = 0; // Count the number of <term>'s
	int currentTermCount = ++termCount;

	char const* Ferr =
		"<factor> does not start with 'IDENT' | 'INT_LIT' | '('";

	level = level + 1;
	cout << spaces() << "enter <term> " << currentTermCount << endl;

	// We next expect to see a <factor>
	if (isFirstOfFactor())
		rtrnvalue1 = factor();
	else
		throw Ferr;

	// As long as the next token is * or /, keep parsing <factor>'s
	while (nextToken == TOK_MULTIPLY || nextToken == TOK_DIVIDE)
	{
		cout <<spaces()<< "-->found: " << yytext << endl;
		int nextTokenLast = nextToken;
		nextToken = yylex();
		if (isFirstOfFactor())
			rtrnvalue2 = factor();
		else
			throw Ferr;

		if (nextTokenLast == TOK_MULTIPLY)
			rtrnvalue1 = rtrnvalue1 * rtrnvalue2;
		else if (nextTokenLast == TOK_DIVIDE)
			rtrnvalue1 = rtrnvalue1 / rtrnvalue2;
	}

	cout << spaces() << "exit <term> " << currentTermCount << endl;
	level = level - 1;
	return rtrnvalue1;

}

//*****************************************************************************
// <factor> --> IDENT | INT_LIT | (
float factor()
{
	float rtrnvalue = 0;
	static int factorCount = 0; // Count the number of <factor>'s
	int currentFactorCount = ++factorCount;
	level = level + 1;
	cout << spaces() << "enter <factor> " << currentFactorCount << endl;

	if (nextToken == TOK_NOT || nextToken == TOK_MINUS)
	{
		cout <<spaces() <<"-->found " << yytext << endl;
		nextToken = yylex();
	}

	//checking for unary
	if (isFirstOfUnary())
	{
		rtrnvalue = unary();
	}
	else
		throw "missing unary at the end";

	cout << spaces() << "exit <factor> " << currentFactorCount << endl;
	level = level - 1;

	return rtrnvalue;
}
float unary()
{
	float rtrnvalue = 0;
	SymbolTableT::iterator it;
	static int unaryCount = 0;
	int currentUnaryCount = ++unaryCount;
	char const *Terr =
		"expression does not start with 'not' | '-' | '(' | 'ID' | 'INTLIT'";
	level = level + 1;
	cout << spaces() << "enter <unary>" << currentUnaryCount << endl;



	// Determine what token we have
	switch (nextToken)
	{
	case TOK_IDENTIFIER:
		cout << spaces() <<"-->found ID: " << yytext << endl;
		it = SymbolTable.find(yytext);
		if (it == SymbolTable.end())
			throw "uninitialized identifier used in expression";

		rtrnvalue = it->second;

		nextToken = yylex(); //Read past what we have found

		break;

	case TOK_INTLIT:
		cout << spaces() << "-->found INT_LIT: " << yytext << endl;
		rtrnvalue = (int)atoi(yytext);

		nextToken = yylex();
		break;

	case TOK_OPENPAREN:
		// We expect ( <expr> )
		//      parse it
		cout << spaces() << "-->found " << endl;
		nextToken = yylex();
		if (isFirstOfExpr()) // Check for 'IDENT' | 'INT_LIT' | (
			rtrnvalue = expr();
		else
			throw Terr;

		if (nextToken == TOK_CLOSEPAREN) {
			cout << spaces() << "-->found " << endl;
			nextToken = yylex();
		}
		else
			throw "<expr> does not end with )";
		break;

	default:
		// If we made it to here, syntax error
		throw "<factor> does not start with 'IDENT' | 'INT_LIT' | '('";
	}

	cout << spaces() << "exit <unary> " << currentUnaryCount << endl;
	level = level - 1;
	return rtrnvalue;
}

void get()
{
	static int getCount = 0;
	int currentGetCount = ++getCount;
	level = level + 1;
	cout << spaces() << "enter <get> " << currentGetCount << endl;

	//"read" is already found so	
	cout << spaces() << "-> found: " << yytext << endl;

	nextToken = yylex();

	//analyzing if the next token is a string literal
	if (nextToken == TOK_STRINGLIT){
	
		cout << spaces() << "-->found string: " << yytext << endl;
		nextToken = yylex();
	}

	//next token should be an identifier
	if (nextToken == TOK_IDENTIFIER) {
		cout << spaces() << "-->found ID: " << yytext << endl;
		string IDname = yytext;
		SymbolTableT::iterator it = SymbolTable.find(IDname);
		if (it == SymbolTable.end()) {
			SymbolTable.insert(pair<string, float>(IDname, 0.0));
		}
	}
else {
	throw "missing identifier in read statement";
	}
	nextToken = yylex();

	//checking for ';'	
	if (nextToken == TOK_SEMICOLON) {
		cout << spaces() << "-->found " << yytext << endl;
	}
	else{
		throw "no ';' at the end";
		}
	nextToken = yylex();
	cout << spaces() << "exit <get>" << currentGetCount << endl;
	level = level - 1;
}

void output() {
	static int outputCount = 0;
	int currentOutputCount = ++outputCount;
	level = level + 1;
	cout << spaces() << "enter <output> " << currentOutputCount << endl;
	cout << spaces() <<"-->found " << yytext << endl;


	nextToken = yylex();
	//checking for Stringlit

	if (nextToken == TOK_STRINGLIT)
	{
		cout << spaces() <<"-->found string: " << yytext << endl;
		nextToken = yylex();
	}

	if (nextToken == TOK_IDENTIFIER)
	{
		cout << spaces() <<"-->found ID: " << yytext << endl;
		string IDname = yytext;

		SymbolTableT::iterator it = SymbolTable.find(IDname);
		if (it == SymbolTable.end()) {
				throw "uninitialized identifier in print statement";
		}
		
		nextToken = yylex();
	}


	//checking for semicolon
	if (nextToken == TOK_SEMICOLON) {
		cout << spaces() <<"-->found ;" << endl;
	}
	else
		throw "missing ';' at the end";
	
	cout << spaces() << "exit <output> " << currentOutputCount << endl;
	level = level - 1;
	nextToken = yylex();
}

void condition()
{
	static int conditionCount = 0;
	int currentConditionCount = ++conditionCount;
	level = level + 1;
	cout << spaces() << "enter <condition> " << currentConditionCount << endl;

	//we already have if..
	cout << spaces() << "-->found " << yytext << endl;

	//checking for openparen

	nextToken = yylex();

	if (nextToken == TOK_OPENPAREN)
	{
		cout << spaces() << "-->found ( " << endl;
		nextToken = yylex();
	}
	else
		throw "missing '(' after 'if'";

	//checking for expr
	if (isFirstOfExpr())
		expr();
	else
		throw "missing 'expr' after '(' ";


	//checking for closeparen

	if (nextToken == TOK_CLOSEPAREN)
	{
		cout << spaces() << "-->found ) " << endl;
		nextToken = yylex();
	}
	else
		throw "missing ')' after expr";



	//checking for program
	if (isFirstOfProgram()) {
		program();
	}
	else
		throw "missing program";

	if (nextToken == TOK_ELSE)
	{
		cout << spaces() << "-->found " << yytext << endl;
		nextToken = yylex();

		//checking for program
		if (isFirstOfProgram())
			program();
		else
			throw "missing 'program' after else";

	}

	cout << spaces() << "exit <conditional> " << currentConditionCount << endl;
	level = level - 1;

}
void whilee()
{
	static int whileeCount = 0;
	int currentWhileeCount = ++whileeCount;
	
	level = level + 1;
	cout << spaces() << "enter <whilee> " << currentWhileeCount << endl;


	cout << spaces() << "-->found " << yytext << endl;
	nextToken = yylex();

	//Checking for openparen
	if (nextToken == TOK_OPENPAREN)
	{
		cout << spaces()<< "-->found " << endl;
		nextToken = yylex();
	}
	else
		throw "missing '(' after 'while' ";

	//cheking for expr
	if (isFirstOfExpr())
	{
		expr();
	}
	else
		throw "missing 'E' after '('";

	//checking for closeparen
	if (nextToken == TOK_CLOSEPAREN)
	{
		cout << spaces() << "-->found " << endl;
		nextToken = yylex();
	}
	else
		throw "missing ')' after expr";

	//checking for program
	if (isFirstOfProgram())
	{
		program();
	}
	else
		throw "missing 'program' after ')'";

	cout << spaces() << "exit <whilee>" << currentWhileeCount << endl;
	level = level - 1;

}






//*****************************************************************************

bool isFirstOfUnary()
{
	return nextToken == TOK_OPENPAREN ||
		nextToken == TOK_IDENTIFIER ||
		nextToken == TOK_INTLIT;
}

bool isFirstOfBoolean()
{
	return  nextToken == TOK_NOT ||
		nextToken == TOK_MINUS ||
		nextToken == TOK_INTLIT ||
		nextToken == TOK_OPENPAREN ||
		nextToken == TOK_IDENTIFIER;
}
bool isFirstOfRelation()
{
	return nextToken == TOK_NOT ||
		nextToken == TOK_MINUS ||
		nextToken == TOK_OPENPAREN ||
		nextToken == TOK_IDENTIFIER ||
		nextToken == TOK_INTLIT;
}

bool isFirstOfExpr() {
	return nextToken == TOK_NOT ||
		nextToken == TOK_MINUS ||
		nextToken == TOK_OPENPAREN ||
		nextToken == TOK_IDENTIFIER ||
		nextToken == TOK_INTLIT;
}

//*****************************************************************************
bool isFirstOfTerm() {
	return nextToken == TOK_NOT ||
		nextToken == TOK_MINUS ||
		nextToken == TOK_OPENPAREN ||
		nextToken == TOK_IDENTIFIER ||
		nextToken == TOK_INTLIT;
}
//*****************************************************************************
bool isFirstOfFactor() {
	return nextToken == TOK_NOT ||
		nextToken == TOK_MINUS ||
		nextToken == TOK_OPENPAREN ||
		nextToken == TOK_IDENTIFIER ||
		nextToken == TOK_INTLIT;
}

bool isFirstOfProgram() {
	return nextToken == TOK_OPENBRACE;
}

bool isFirstOfStatement() {
	return nextToken == TOK_LET ||
		nextToken == TOK_READ ||
		nextToken == TOK_IF ||
		nextToken == TOK_PRINT ||
		nextToken == TOK_WHILE;
}
bool isFirstOfAssignment() {
	return nextToken == TOK_LET;
}
bool isFirstOfOutput() {
	return nextToken == TOK_PRINT;

}
bool isFirstOfWhilee() {
	return nextToken == TOK_WHILE;

}
bool isFirstOfGet() {
	return nextToken == TOK_READ;
}
bool isFirstOfCondition() {
	return nextToken == TOK_IF;
}
//*****************************************************************************
string spaces() {
	string str(level * 3, ' ');
	return str;
}

#endif


