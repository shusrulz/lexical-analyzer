/******************************************************************* 
Your file header information....

Feel free to split your file into .h and .cpp, but you will need
to modify the makefile for the project
*******************************************************************/
#ifndef PRODUCTIONS_H
#define PRODUCTIONS_H

extern int nextToken;
extern int level;
extern set<string> symbolTable;

extern "C"
{
	// Instantiate global variables used by flex
	extern char* yytext;       // text of current lexeme
	extern int   yylex();      // the generated lexical analyzer
}
//prior function declaration
void program();
void block();
void expr();
void statement();
void assignment();
void compoundStatement();
void begin();
void iff();
void while_();
void read();
void write();
void simple_expr();
void term();
void factor();


bool first_of_program();
bool first_of_block();
bool first_of_statement();
bool first_of_compound_statement(void);
bool is_simple_expr(void);
bool is_term(void);
bool is_factor(void);
bool is_defined(void);

string spaces() {
	string str(level * 4, ' ');
	return str;
}

void output_lexeme(){
    cout << spaces() << "-->found " << yytext << endl;
}

void program() {
    ++level;
    cout << spaces() << "enter <program>" << endl;
    
    if (nextToken == TOK_PROGRAM){
        output_lexeme();
        nextToken = yylex();
        if (nextToken == TOK_IDENT){
            output_lexeme();
            nextToken == yylex();
            if (nextToken == TOK_IDENT){
                output_lexeme();
                nextToken == yylex();
            }else{
                throw "14: ';' expected";
            }
        }else{
            throw "2: identifier expected";
        }
        }else{
            throw "3: 'PROGRAM' expected";
    }
    if (first_of_block){
        block();
    }
    if (nextToken==TOK_END){
        nextToken = yylex();
    }
    cout << spaces() << "exit <program>" << endl;
    --level;
}

void block(){
    ++level;
	cout << spaces() << "enter <block> "<<endl;
   
    //even the yytext is VAR it gives TOK_IDENTIFIER
    //May be some error in the rules.l
    //makeVar here
    if (nextToken == TOK_IDENT){ //make TOK_VAR instead of TOK_IDENT here to pass for the error.pas
        output_lexeme();
        cout<<"here even the token is VAR the output is TOK_IDENT,SO,I am getting error, if I write TOK_IDENT in above condition it will pass error2 and sample"<<endl;
        nextToken == yylex();
        if(nextToken != TOK_IDENT){
            throw "2: identifier expected";
        }
        while (nextToken == TOK_IDENT){
            set<string>::iterator it;
            for (it = symbolTable.begin(); it != symbolTable.end(); ++it) {
                if (*it == yytext){
                    throw "101: identifier declared twice";
                }
            }
            symbolTable.insert(yytext);
            output_lexeme();
            nextToken == yylex();
            if (nextToken != TOK_IDENT){//make COlon here;
                throw "5: ':' colon";
            }
            output_lexeme();
            nextToken = yylex();
            if (nextToken != TOK_REAL && nextToken!=TOK_INTEGER){//make Real or integer here;
                throw "10: error in type variable declaration other than INTEGER or REAL";
            }
            output_lexeme();
            nextToken = yylex();
            if (nextToken != TOK_SEMICOLON){
				throw "5: ':' expected";
			}
            output_lexeme();
            nextToken = yylex();
        }
    }
    // else{
     //also for tok begin I am getting 4000 which is value for tok identifier. might be error in rules.l   
    //     if (nextToken != TOK_BEGIN){
    //         throw "17: 'BEGIN' expected <block> does not start with VAR or BEGIN"; //uncomment and see if its working BEGIN doesn't return tok_begin
    //      }
    // }
    while(first_of_compound_statement){
        if (nextToken == TOK_END){
            break;
        }
		compoundStatement();
	}

	if (nextToken != TOK_END){
		throw "13: 'END' expected";
	}
	cout<< spaces()<<"exit <block>"<<endl;
	level = level - 1; 
}

void compoundStatement(){
    ++level;
    cout << spaces() << "enter <compound statement> " <<endl;
    output_lexeme();
    nextToken = yylex();
    while (first_of_statement){
        statement();
        output_lexeme();
        if (nextToken != TOK_END){
            nextToken = yylex();
            //output_lexeme();
        }else{
             break;
        }
    }
    cout << spaces() << "exit <compound statement> " <<endl;
    level = level -1;
}

void statement(){
    ++level;
    cout << spaces() << "enter <statement> " <<endl;
    switch (nextToken) {
	case TOK_IDENT:
		assignment();
		break;
	case TOK_IF:
		iff();
		break;
	case TOK_WHILE:
		while_();
		break;
	case TOK_READ:
		read();
		break;	
	case TOK_WRITE:
		write();
		break;	
	default:
		throw "900: illegal type of statement, illegal first of statement";
	}
    cout << spaces() << "exit <statement> " <<endl;
	level = level - 1;
}

void assignment(){
    float finalReturnVal = 0;
	++level;
	cout << spaces() << "enter <assignment> "<< endl;
    output_lexeme();
    nextToken = yylex();
    if (nextToken == TOK_ASSIGN){
        output_lexeme();
        nextToken = yylex();
        expr();
    }
    cout << spaces() << "exit <assignment> "<< endl;
	level = level - 1;
}

void read(){
    ++level;
    cout << spaces() << "enter <read> "<< endl;
    output_lexeme();
    nextToken = yylex();
    if (nextToken==TOK_OPENPAREN){
        output_lexeme();
        nextToken = yylex();
        if (nextToken == TOK_IDENT){
            output_lexeme();
            nextToken = yylex();
            if (nextToken == TOK_CLOSEPAREN){
                output_lexeme();
                nextToken = yylex();
            }else{
                throw "4: ')' expected";
            }
        }else{
            throw "134: illegal type of operand(s)";
            }
        }else{
        throw "9: '(' expected ";
    }
    if (nextToken!=TOK_SEMICOLON){
        throw "14: ';' expected";
    }
    cout << spaces() << "exit <read>"<< endl;
    level = level - 1;

}

void write(){
    ++level;
    cout << spaces() << "enter <write> "<< endl;
    output_lexeme();
    nextToken = yylex();
    if (nextToken==TOK_OPENPAREN){
        output_lexeme();
        nextToken = yylex();
        if (nextToken == TOK_IDENT||nextToken == TOK_STRINGLIT){
            output_lexeme();
            nextToken = yylex();
            if (nextToken == TOK_CLOSEPAREN){
                output_lexeme();
                nextToken = yylex();
            }else{
                throw "4: ')'";
            }
        }else{
            throw "134: illegal type of operand(s) WRITE does not contain an identifier or string literal";
            }
        }else{
        throw "9: '(' expected ";
    }
    if (nextToken!=TOK_SEMICOLON && nextToken!=TOK_END && nextToken != TOK_ELSE){
        throw "14: ';' expected";
    }
    cout << spaces() << "exit <write>"<< endl;
    level = level - 1;


}
void iff(){
    ++level;
    cout << spaces() << "enter <if> "<< endl;
    output_lexeme();
    nextToken = yylex();
    expr();
    if (nextToken == TOK_THEN){
        output_lexeme();
        nextToken =yylex();
        statement();        
    }else{
        throw "52: 'THEN' expected";
    }
    cout << spaces() << "exit <if> "<< endl;
	level = level - 1;

}

void while_(){
    ++level;
    cout << spaces() << "enter <while> "<< endl;
    output_lexeme();
    nextToken = yylex();
    expr();
    statement();
    cout << spaces() << "exit <while> "<< endl;
	level = level - 1;

}

//*****************************************************************************
void expr(){
    ++level;
	cout << spaces() << "enter <expression> "<< endl;
    simple_expr();
    
    if (is_simple_expr()){
        nextToken = yylex();
        simple_expr();
    }

    cout << spaces() << "exit <expression> "<< endl;
	level = level-1;
}

void simple_expr(){
 	++level;
	cout << spaces() << "enter <simple expression> "<< endl;
    term();
    while (is_term()){
        output_lexeme();
        nextToken = yylex();
        term();
    }
	cout << spaces() << "exit <simple expression> "<< endl;
	level = level-1;
}


void term(){
    ++level;
	cout << spaces() << "enter <term> "<< endl;
	factor();
    while (nextToken==TOK_MULTIPLY||nextToken == TOK_DIVIDE|| nextToken == TOK_AND){
        output_lexeme();
        nextToken = yylex();
        factor();
    }
	cout << spaces() << "exit <term> "<< endl;
	level = level-1;
}
//=================================================================
void factor(){
    ++level;
	cout << spaces() << "enter <factor> "<< endl;
    if (is_factor()){
        output_lexeme();
        if (!is_defined()&& nextToken!=TOK_INTLIT && nextToken!= TOK_FLOATLIT && nextToken!= TOK_OPENPAREN && nextToken!= TOK_CLOSEPAREN){
            throw "104: identifier not declared";
        }
        
        //symboltable
        if (nextToken == TOK_OPENPAREN){
            nextToken = yylex();
			expr();
            nextToken = yylex();
            if (nextToken== TOK_CLOSEPAREN){
                output_lexeme();
				nextToken = yylex();
            }else{
                throw "4: ')' expected";
            }
        }
        if (nextToken==TOK_NOT || nextToken == TOK_MINUS){
            nextToken = yylex();
            factor();
        }

            
            
    }else{
        throw "expected factor";
    }
    nextToken = yylex();
    cout << spaces() << "exit <factor> "<< endl;
	level = level-1;
}

bool first_of_program(void) {
    return nextToken == TOK_PROGRAM;
}
bool first_of_block(void) {
    return (nextToken == TOK_VAR || nextToken == TOK_BEGIN);
}
bool first_of_compound_statement(void){
	return (nextToken==TOK_BEGIN);
}
bool first_of_statement(void){
	return (nextToken == TOK_IDENT||nextToken == TOK_BEGIN||
		   nextToken == TOK_IF||nextToken == TOK_WHILE||
		   nextToken == TOK_READ||nextToken == TOK_WRITE);
}

bool is_simple_expr(void){
    return (nextToken == TOK_GREATERTHAN||nextToken == TOK_LESSTHAN||
            nextToken == TOK_EQUALTO || nextToken == TOK_NOTEQUALTO);
}

bool is_term(void){
    return (nextToken == TOK_PLUS||nextToken == TOK_MINUS||
            nextToken == TOK_OR);
}

bool is_factor(){
	return (nextToken == TOK_INTLIT|| nextToken == TOK_STRINGLIT||
		nextToken == TOK_IDENT || nextToken == TOK_OPENPAREN ||
		nextToken == TOK_NOT || nextToken == TOK_MINUS|| TOK_FLOATLIT );
}

bool is_defined(){
    set<string>::iterator it;
    for (it = symbolTable.begin(); it != symbolTable.end(); ++it) {
        if (*it == yytext){
            return true;
            }
        }
    return false;    
}
#endif
