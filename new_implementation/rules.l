/*Name:Bhabish subedi     NETID: bs2321
  Course: 4713            Assignment: Part 1
  Programming Environment:WSL C++
  Purpose of the file: Our main part for this project is to write the  lexical  analyzer for a subnet of TIPS.*/


/******************************************************************* 
Starting point your rules.l file for TIPS
*******************************************************************/


%option noyywrap
%{
  
#include "lexer.h"

// global variable to hold current line number being read

int line_number = 1;

%}

%%
 
\n line_number++;

 /* Keywords */ 

BEGIN   {return TOK_BEGIN;}
BREAK { return TOK_BREAK;}
CONTINUE {return TOK_CONTINUE;}
DOWNTO {return TOK_DOWNTO; }
ELSE {return TOK_ELSE;}
END {return TOK_END;}
FOR {return TOK_FOR;}
IF {return TOK_IF;}
LET {return TOK_LET;}
PROGRAM {return TOK_PROGRAM;}
READ {return TOK_READ;}
THEN {return TOK_THEN;}
TO {return TOK_TO;}
VAR {return TOK_VAR;}
WHILE { return TOK_WHILE;}
WRITE { return TOK_WRITE;}


 /* Datatype Specifiers */

 INTEGER {return TOK_INTEGER;}
 REAL {return TOK_REAL;}



 /* Punctuation */

\; { return TOK_SEMICOLON;}
\:  { return TOK_COLON; }
\( {return TOK_OPENPAREN;}
\) {return TOK_CLOSEPAREN;}
\{ {return TOK_OPENBRACE;}
\} {return TOK_CLOSEBRACE;}


 /* Operators */

 \+ {return TOK_PLUS;}
 \- {return TOK_MINUS;}
 \* {return TOK_MULTIPLY;}
 \/ {return TOK_DIVIDE;}
 \:= {return TOK_ASSIGN;}
 \= {return TOK_EQUALTO;}
 \< {return TOK_LESSTHAN;}
 \> {return TOK_GREATERTHAN;}
 \<> {return TOK_NOTEQUALTO;}
 MOD {return TOK_MOD;}
 NOT {return TOK_NOT;}
 OR {return TOK_OR;}
 AND {return TOK_AND;}


 /* Abstractions */

[A-Z][A-Z0-9]{0,7} {return TOK_IDENT;} 
 
 /* Regular expression for an identifier starts with a variable that is small or capital followed by variable or constant. Similarly, identifier has atmost 8 charcaters.*/

[0-9]+ {return TOK_INTLIT;} 

 /* Regualr expression for an interger is the numbers form 0 to 9 followed by other numbers.*/

[0-9]+.[0-9]+ {return TOK_FLOATLIT;}

 /* Regualr expression for Float is number from 0 to 9 followed by another  numbers if its there is any followed by dot sign and numbers from 0 to 9 and numbers another number if possible */

['][^']{0,80}['] {return TOK_STRINGLIT;} 

 /* Regualr expression for a string is it includes everything which is denoted by ' sign. Also we have [^'] which denotes non digit such as [^1-4]. Also, i takes less than 80 characters. */


 /* Eat any whitespace */

[ \t\r]+               /* nop */


 /* Found an unknown character */

.         { return TOK_UNKNOWN; }


 /* Recognize end of file */

<<EOF>>   { return TOK_EOF; }



