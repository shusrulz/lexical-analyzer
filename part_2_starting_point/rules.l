/******************************************************************* 
Your file header information....

Reuse your rules.l file from Part 1
*******************************************************************/
%option noyywrap
%{
#include "lexer.h"

// global variable to hold current line number being read
int line_number = 1;

%}

%%

 /* Keywords */ 


 /* Datatype Specifiers */


 /* Punctuation */


 /* Operators */


 /* Abstractions */



 /* Eat any whitespace */

[ \t\r]+               /* nop */


 /* Found an unknown character */

.         { return TOK_UNKNOWN; }

 /* Recognize end of file */

<<EOF>>   { return TOK_EOF; }


