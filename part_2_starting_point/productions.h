/******************************************************************* 
Your file header information....

Feel free to split your file into .h and .cpp, but you will need
to modify the makefile for the project
*******************************************************************/
#ifndef PRODUCTIONS_H
#define PRODUCTIONS_H

extern int nextToken;
extern int level;
extern set<string> symbolTable;

extern "C"
{
	// Instantiate global variables used by flex
	extern char* yytext;       // text of current lexeme
	extern int   yylex();      // the generated lexical analyzer
}

string spaces() {
	string str(level * 4, ' ');
	return str;
}

void output_lexeme(){
    cout << spaces() << "-->found " << yytext << endl;
}

void program() {
    
    ++level;
    cout << spaces() << "enter <program>" << endl;

    // Parse input to see if the input matches the EBNF rule for <program>
    

    cout << spaces() << "exit <program>" << endl;
    --level;
}


//*****************************************************************************

bool first_of_program(void) {
    return nextToken == TOK_PROGRAM;
}


#endif
