/*********************************************************************Name: Roshan KC
Net ID: rk942
Course: CSE 4713 Programming Language
Assignment: Part1 Programming
Environment: WSL C++
Purpose of File: rules file  *********************************************************************/
%option noyywrap
%{
#include "lexer.h"

// global variable to hold current line number being read
int line_number = 1;
int a = 0;

%}

%%

; \n  {line_number++;
;       if (a >0){
;       line_number = line_number+a;
;       a=0;
;       }
; }

 /* Keywords */
IF            {return TOK_IF; }
ELSE            {return TOK_ELSE; }
WHILE          {return TOK_WHILE; }
FOR            {return TOK_FOR; }
BEGIN         { return TOK_BEGIN;}
BREAK         {return TOK_BREAK;}
CONTINUE      {return TOK_CONTINUE;}
DOWNTO        {return TOK_DOWNTO;}
END           {return TOK_END;}
LET           {return TOK_LET;}
PROGRAM       {return TOK_PROGRAM;}
READ          {return TOK_READ;}
THEN          {return TOK_THEN;}
TO            {return TOK_TO;}
VAR           {return TOK_VAR;}
WRITE         {return TOK_WRITE;}



 /* Datatype Specifiers */
INTEGER       {return TOK_INTEGER; }
REAL       {return TOK_REAL; }




 /* Punctuation */

\(        { return TOK_OPENPAREN; }
\)        { return TOK_CLOSEPAREN; }

:         { return TOK_COLON; }
;         { return TOK_SEMICOLON; }
\{        {return TOK_OPENBRACE;}
\}        {return TOK_CLOSEBRACE;}

 /* Operators */

 \+        { return TOK_PLUS; }
-         { return TOK_MINUS; }
\*        { return TOK_MULTIPLY; }
\/        { return TOK_DIVIDE; }

:=        {return TOK_ASSIGN;}
=         {return TOK_EQUALTO;}
\<         {return TOK_LESSTHAN;}
\>         {return TOK_GREATERTHAN;}
\<>        {return TOK_NOTEQUALTO;}
MOD       {return TOK_MOD;}
NOT       {return TOK_NOT;}
OR        {return TOK_OR;}
AND       {return TOK_AND;}

 /* Abstractions */
[A-Z][0-9A-Z]{0,7}   { return TOK_IDENT; }
[0-9]+                 { return TOK_INTLIT; }
[0-9]+\.[0-9]+ {return TOK_FLOATLIT;}

'[^']{0,80}'  {for (int i = 0; i <= yyleng;i++){
	                 if(yytext[i] == '\n')
		               a++;
                   }
                   return TOK_STRINGLIT;  }


[ \t\r]+              /* nop */


 /* Found an unknown character */

.         { return TOK_UNKNOWN; }


 /* Recognize end of file */

<<EOF>>   { return TOK_EOF; }

'[^']{0,80} {return TOK_EOF_SL;}
