//*****************************************************************************
// purpose: CSE 4713 / 6713 Assignment 1: Lexical Analyzer
// created: 9-3-2019
//*****************************************************************************
#include "lexer.h"
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include "switch_flags.h"
//*****************************************************************************
// Do the lexical parsing
char lexeme[MAX_LEXEME_LEN];  // Character buffer for lexeme



// Defined Function to change string value to integer value
constexpr unsigned int string2int(const char* str, int h = 0)
{
  //hash function
 return !str[h] ? 5381 : (string2int(str, h+1)*33) ^ str[h];
}


	// function which checks the token value of the lexeme entered
int checkLexemeValue(char* input)
{
  int token_value = 0;
  //unsigned int string2int(const char* str, int h = 0);
  switch (string2int(input))
  {
    case string2int("if")://from here check for valid keywords
      token_value = TOK_IF;
      break;
    case string2int("else"):
      token_value = TOK_ELSE;
      break;
    case string2int("for"):
      token_value = TOK_FOR;
      break;
    case string2int("while"):
      token_value = TOK_WHILE;
      break;
    case string2int("print"):
      token_value = TOK_PRINT;
      break;
    case string2int("return"):
      token_value = TOK_RETURN;
      break;
    case string2int("continue"):
      token_value = TOK_CONTINUE;
      break;
    case string2int("break"):
      token_value = TOK_BREAK;
      break;
    case string2int("debug"):
      token_value = TOK_DEBUG;
      break;
    case string2int("read"):
      token_value = TOK_READ;
      break;
    case string2int("let"):
      token_value = TOK_LET;
      break;
    case string2int("int"): // from here datatype specifiers
      token_value = TOK_INT;
      break;
    case string2int("float"):
      token_value = TOK_FLOAT;
      break;
    case string2int("string"):
      token_value = TOK_STRING;
      break;
    case string2int(";"):// from here check for punctuations
      token_value = TOK_SEMICOLON;
      break;
    case string2int("("):
      token_value = TOK_OPENPAREN;
      break;
    case string2int(")"):
      token_value = TOK_CLOSEPAREN;
      break;
    case string2int("{"):
      token_value = TOK_OPENBRACE;
      break;
    case string2int("}"):
      token_value = TOK_CLOSEBRACE;
      break;
    case string2int("["):
      token_value = TOK_OPENBRACKET;
      break;
    case string2int("]"):
      token_value = TOK_CLOSEBRACKET;
      break;
    case string2int(","):
      token_value = TOK_COMMA;
      break;
    case string2int("+"):// operators from here
      token_value = TOK_PLUS;
      break;
    case string2int("-"):
      token_value = TOK_MINUS;
      break;
    case string2int("*"):
      token_value = TOK_MULTIPLY;
      break;
    case string2int("/"):
      token_value = TOK_DIVIDE;
      break;
    case string2int(":="):
      token_value = TOK_ASSIGN;
      break;
    case string2int("=="):
      token_value = TOK_EQUALTO;
      break;
    case string2int("<"):
      token_value = TOK_LESSTHAN;
      break;
    case string2int(">"):
      token_value = TOK_GREATERTHAN;
      break;
    case string2int("and"):
      token_value = TOK_AND;
      break;
    case string2int("or"):
      token_value = TOK_OR;
      break;
    case string2int("not"):
      token_value = TOK_NOT;
      break;
    case string2int("len"):
      token_value = TOK_LENGTH;
      break;
    default:
      token_value = TOK_UNKNOWN;
      break;
}
  return token_value;
}


int evaluation(char* input)
{
  int token_value = 0;

  // Initialize boolean varaible to check the correct datatype of the token
  bool checkForDecimal = 0;
  bool checkForLetter = 0;
  bool checkForNumber = 1;
	//assign and instantiate i with 0 value
  int i =0;
//run while loop until MAX_LEXEME_LEN times
  while(i < MAX_LEXEME_LEN)
  {
    while (isdigit(input[i]) == 0)
    {
      //Assigning boolean values to check the datatype of the input
      bool conditionForDecimal = (input[i] == '.');
      bool conditionforEscape = (input[i] == '\0');
      bool conditionForAlphabet = (input[i] != 0);
      int numberForSwitch = 0;    

      // Storing each boolean variables with consition to a numberForSwitch integer variable 
      if(conditionForDecimal)
      {
        numberForSwitch = 1;
      }
      else if(conditionforEscape)
      {
        numberForSwitch = 2;
      }
      else if(conditionForAlphabet)
      {
        numberForSwitch = 3;
      }
      else
      {
        numberForSwitch = 4;
      }
      //Using switch case to act on the input for case of different datatypes
      switch(numberForSwitch)
      {
        case 1:
        checkForDecimal = 1;
        case 2:
        break;
        case 3:
        checkForNumber = 0;
        checkForLetter = 1;
        break;
        case 4:
        checkForNumber = 0;
        break;
      }
    }
    i=i+1;
  }
// ternary operator use for calculating checkDecimal boolean value
  bool checkDecimal = (checkForDecimal)?TOK_FLOATLIT
            :TOK_INTLIT;
//ternary operator use for calculating checkLetter boolean value
  bool checkLetter = (checkForLetter)?TOK_IDENTIFIER
            :TOK_UNKNOWN;
  // assigning token value with the use of ternary operator
  token_value = (checkForNumber)?checkDecimal
          :checkLetter;
}

int yylex()
{
  // assign 0 to token value
  int token_value = 0;
  // assign -1 to c
  static char c = -1;         // Use for chaecking the initial character

  // Using for loop for holding the lexeme.
  for( int i = 0; i < MAX_LEXEME_LEN; i++ )
    lexeme[i] = '\0';
  yytext = lexeme;
  yyleng = 0;

  // Check the initial character of the lexeme
  if( c < 0 )
    c = fgetc( yyin );

  // Checkinh weather it is the end of lexeme.
  if( feof(yyin) ) {
    c = -1;
    return( TOK_EOF );
  }
// Assign is_string_input to boolean value
  bool is_string_input = (c == '"');

	// Checking if evaluation is complete or not with a boolean value
  bool checkEvaluationCompletion = 0;

  // Repeat for every character present in the lexeme with the use of while loop
  while (checkEvaluationCompletion == 0)
  {
    int nullValue = 0;
    int horizntalTabValue = 9;
    int backSpaceValue = 10;
    int verticalTabValue = 11;
    int newPageValue =12;
    int carriageReturnValue = 13;
    int spaceValue = 32;
    bool checkCondition =  (c == nullValue || c == horizntalTabValue || c == backSpaceValue || c == verticalTabValue || c == newPageValue || c == carriageReturnValue || c == spaceValue);
		// Checking if there are whitespace characters in the lexeme.
    while (!is_string_input && (c == nullValue || c == horizntalTabValue || c == backSpaceValue || c == verticalTabValue || c == newPageValue || c == carriageReturnValue || c == spaceValue))
    {
      c = fgetc(yyin);
      continue;
    }

    lexeme[yyleng++] = c;
    c = fgetc( yyin );

	// Used to check weather the input is string datatype or not
    if (is_string_input)
    {
    //Assigning boolean values for condition in input and end of file case
    bool conditionForEndOFFile = (feof(yyin));
    bool conditionForInput = (c== '"');

    //Initializing conditionNumber for switch case
    int conditionNumber =0;
    
    //If else llop for assigning conditionNumber value according to conditions
    if(conditionForEndOFFile)
    {
      conditionNumber = 1;
    }
    else if(conditionForInput)
    {
      conditionNumber = 2;
    }

    // Using switch case for two different conditions
    switch(conditionNumber)
    {
      case 1:
         c = -1;
        return TOK_EOF_SL;
      case 2:
        //Assign token_value with the value in TOK_STRAINGLIT
        token_value = TOK_STRINGLIT;
        lexeme[yyleng++] = c;
        c = fgetc(yyin);
        while (c == nullValue || c == horizntalTabValue || c == backSpaceValue || c == verticalTabValue || c == newPageValue || c == carriageReturnValue || c == spaceValue)
        {
          c = fgetc(yyin);
        }
        checkEvaluationCompletion = 1;     
    }
    }
    else
    {
      token_value = checkLexemeValue(lexeme);
      int not_necessary;
      if (token_value == TOK_UNKNOWN)
      {
        //Assigning boolean variables for different conditions
        bool conditionForPunctuation = (c == nullValue || c == horizntalTabValue || c == backSpaceValue || c == verticalTabValue || c == newPageValue || c == carriageReturnValue || c == spaceValue);
        bool conditionForOtherThanPunctuation = (ispunct(c) != 0);
        bool conditionForIntAndFloat = (evaluation(lexeme) == TOK_INTLIT || evaluation(lexeme) == TOK_FLOATLIT);
        bool conditionForUnknownToken = (evaluation(lexeme) == TOK_UNKNOWN);

        // Assigning condition number to different conditions
        int conditionNumber = 0;
        if(conditionForPunctuation)
        {
          conditionNumber = 1;
        }
        else if(conditionForOtherThanPunctuation)
        {
          conditionNumber = 2;
        }
        else if(conditionForIntAndFloat)
        {
          conditionNumber = 3;
        }
        else if(conditionForUnknownToken)
        {
          conditionNumber = 4;
        }

        // Switch case use for different conditions 
        switch(conditionNumber)
        {
          case 1:
          token_value = evaluation(lexeme);
          c = fgetc(yyin);
          checkEvaluationCompletion = 1;
          case 2:
          if (checkLexemeValue(&c) != TOK_UNKNOWN)
          {
            token_value = evaluation(lexeme);
            checkEvaluationCompletion = 1;
          }
          case 3:
          if (isdigit(c) == 0 && c != '.')
          {
            token_value = evaluation(lexeme);
            checkEvaluationCompletion = 1;
          }
          case 4:
          token_value = evaluation(lexeme);
          checkEvaluationCompletion = 1; 
        }              
      }
      else
      {
        if (c == nullValue || c == horizntalTabValue || c == backSpaceValue || c == verticalTabValue || c == newPageValue || c == carriageReturnValue || c == spaceValue)
        {
          c = fgetc(yyin);
          checkEvaluationCompletion = 1;
        }
        else if (checkLexemeValue(lexeme) != TOK_UNKNOWN)
        {
          if (checkLexemeValue(lexeme) == TOK_LESSTHAN)
          {
            if (checkLexemeValue(&c) == TOK_GREATERTHAN)
            {
              lexeme[yyleng++] = c;
              token_value = checkLexemeValue(lexeme);

              c = fgetc(yyin);

              while (c == nullValue || c == horizntalTabValue || c == backSpaceValue || c == verticalTabValue || c == newPageValue || c == carriageReturnValue || c == spaceValue)
              {
                c = fgetc(yyin);
              }

              checkEvaluationCompletion = 1;
            }
            else
              checkEvaluationCompletion = 1;
          }
          else
            checkEvaluationCompletion = 1;
        }
        else if (ispunct(c) != 0)
        {
          while (checkLexemeValue(&c) != TOK_UNKNOWN)
          {
            token_value = checkLexemeValue(lexeme);

            checkEvaluationCompletion = 1;
          }
        }
      }
    }
  }
  return token_value;
}
