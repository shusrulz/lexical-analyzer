/******************************************************************* 
Starting point your rules.l file for TIPS
*******************************************************************/
%option noyywrap
%{
#include "lexer.h"

// global variable to hold current line number being read
int line_number = 1;

%}

%%

 /* Keywords */ 


 /* Datatype Specifiers */


 /* Punctuation */
 
:         { return TOK_COLON; }


 /* Operators */


 /* Abstractions */


 /* Eat any whitespace */

[ \t\r\n]+               /* nop */


 /* Found an unknown character */

.         { return TOK_UNKNOWN; }


 /* Recognize end of file */

<<EOF>>   { return TOK_EOF; }


