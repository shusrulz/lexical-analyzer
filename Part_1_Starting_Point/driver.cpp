//*****************************************************************************
// CSE 4713 / 6713 Project Part 1 - Lexical Analyzer Driver
// Fall 2020
//*****************************************************************************

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include "lexer.h"

// Instantiate global variables
extern "C"
{
extern FILE *yyin;         // input stream
extern FILE *yyout;        // output stream
extern int   yyleng;       // length of current lexeme
extern char *yytext;       // text of current lexeme
extern int   yylex();      // the generated lexical analyzer

extern int   line_number;  // current line number of the input
}

// Do the analysis
int main( int argc, char* argv[] ) {
  int token;   // hold each token code

  // Set the input stream
  if (argc > 1) {
    printf("INFO: Using the file %s for input\n", argv[1]);
    yyin = fopen(argv[1], "r");
    if (!yyin) {
      printf("   ERROR: input file not found\n");
      return (-1);
    }
  }
  else {
    printf("INFO: Using stdin for input, use EOF to end input\n");
    printf("      Windows EOF is Ctrl+z, Linux EOF is Ctrl+d\n");
    yyin = stdin;
  }

  // Set the output stream
  yyout = stdout;

  // Do the lexical parsing
  token = yylex();
  while( token != TOK_EOF )
  {
    // What did we find?
    fprintf(yyout, "line: %d, lexeme: |%s|, length: %d, token: %d\n",
                        line_number, yytext, yyleng, token);

    // Is it an error?
    if( token == TOK_UNKNOWN )
      fprintf(yyout,"   ERROR: unknown token\n");
    if( token == TOK_EOF_SL )
      fprintf(yyout,"   ERROR: end of file while in a string literal\n");

    // Get the next token
    token = yylex();
  }
  return 0;
}
