//*****************************************************************************
// purpose: driver file for flex example
// version: Spring 2020
// author: Joe Crumpton
//*****************************************************************************
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include "lexer.h"


extern "C"
{
// Instantiate global variables
extern FILE *yyin;         // input stream
extern FILE *yyout;        // output stream
extern int   yyleng;       // length of current lexeme
extern char *yytext;       // text of current lexeme
extern int   yylex();      // the generated lexical analyzer
}


// Do the analysis
int main( int argc, char* argv[] )
{
  int token;   // hold each token code

  // Set the input stream
  if (argc > 1) 
  {
    printf("INFO: Using the file %s for input\n", argv[1]);
    yyin = fopen(argv[1], "r");
    if (!yyin)
    {
      printf("   ERROR: input file not found\n");
      return (-1);
    }
  }
  else 
  {
    printf("INFO: Using stdin for input, use EOF to end input\n");
    printf("      Windows EOF is Ctrl+z, Linux EOF is Ctrl+d\n");
    yyin = stdin;
  }

  // Set the output stream
  yyout = stdout;

  // Do the lexical parsing
  token = yylex();
  while( token != TOK_EOF ) 
  {
    // What did we find?
    printf("lexeme: |%s|, length: %d, token: %d\n", yytext, yyleng, token);
    
    // Is it an error?
    if( token == TOK_UNKNOWN )
      printf("   ERROR: unknown token\n");
      
    // Get the next token
    token = yylex();
  }

  return(0);
}
